<div>
    First name:<br>
    <input type="text" name="firstname" wire:model="firstName"><br>
    Last name:<br>
    <input type="text" name="lastname" wire:model="lastName"> <br/>

    <button wire:click="submitData">Submit</button>

    <br/>
    <h1>updated after button click : </h1>
    <h1> {{ $fullname }}</h1>
    =====================================
    <h1>Test Data Binding : </h1>
    <h2> {{ $firstName .' '.$lastName }}</h2>
</div>
