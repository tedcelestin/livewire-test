<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Testcomponent extends Component
{
    public $firstName;
    public $lastName;
    public $fullname ;


    public function submitData()
    {

        $this->fullname = $this->firstName .' '.$this->lastName;

    }

    public function render()
    {
        return view('livewire.testcomponent');
    }
}
